package com.adamkorzeniak.test;

import lombok.ToString;

@ToString
public class TestSubClass {

    private String key;
    private String value;
    private int priority;
    private TestSubSubClass xxx;

}
