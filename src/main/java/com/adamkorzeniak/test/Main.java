package com.adamkorzeniak.test;

import com.adamkorzeniak.object_generator.ObjectGenerator;

public class Main {

    //Napisać testy
    //Zrefaktorować
    //Dodać wsparcie dla zagnieżdzonego patha
    //Dodaj wsparcie na customowe generatory po typie wewnątrz subklasy
        // Pierwszy wewnątrz konkretnej subklasy
        // Drugi wewnątrz subklasy i podsubklas
    //Stwórz sortowanie supplierów
    public static void main(String[] args) {
        ObjectGenerator<TestClass> generator = ObjectGenerator.builder(TestClass.class)
                .addPathSupplier("name", () -> Math.random() > 0.5 ? "Adam" : "Magda")
                .addPathSupplier("year", () -> (int) (Math.random() * 10 + 2000))
                .addTypeSupplier(int.class, () -> (int) (Math.random() * 20))
                .addTypeSupplier(String.class, () -> "abcd")
                .addAcceptedPackage("com.adamkorzeniak")
                .build();

        generator.stream()
                .limit(10)
                .forEach(System.out::println);
    }

}