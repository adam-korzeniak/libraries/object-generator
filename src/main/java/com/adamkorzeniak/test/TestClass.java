package com.adamkorzeniak.test;

import lombok.ToString;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.*;

@ToString
public class TestClass {

    private String name;
    private String details;

    private int age;
    private Integer year;
    private long bigNumber;
    private BigInteger veryBigNumber;

    //TODO:
    private double average;
    //TODO:
    private float smallAverage;
    //TODO:
    private BigDecimal perfectAverage;

    //TODO:
    private LocalDateTime localDateTime;
    //TODO:
    private LocalDate localDate;
    //TODO:
    private LocalTime localTime;
    //TODO:
    private OffsetDateTime offsetDateTime;
    //TODO:
    private OffsetTime offsetTime;

    private TestSubClass inner;

}
