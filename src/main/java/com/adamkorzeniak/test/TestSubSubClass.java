package com.adamkorzeniak.test;

import lombok.ToString;

@ToString
public class TestSubSubClass {

    private String key;
    private String value;
    private int priority;

}
