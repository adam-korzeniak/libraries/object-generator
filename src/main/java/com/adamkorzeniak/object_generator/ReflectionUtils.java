package com.adamkorzeniak.object_generator;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.stream.Stream;

class ReflectionUtils {

    private ReflectionUtils() {
    }

    static Stream<Field> getFields(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> !Modifier.isStatic(field.getModifiers()));
    }
}
