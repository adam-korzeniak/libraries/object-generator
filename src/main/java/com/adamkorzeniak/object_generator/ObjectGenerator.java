package com.adamkorzeniak.object_generator;

import com.adamkorzeniak.object_generator.suppliers.ValueGeneratorService;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ObjectGenerator<T> {

    private final ValueGeneratorService valueGenerator;
    private final Class<T> clazz;

    public ObjectGenerator(Class<T> clazz,
                           Map<String, Supplier<?>> pathSuppliers,
                           Map<Class<?>, java.util.function.Supplier<?>> typeSuppliers,
                           Set<String> acceptedPackages) {
        this.clazz = clazz;
        this.valueGenerator = new ValueGeneratorService(pathSuppliers, typeSuppliers, acceptedPackages);
    }

    public static <T> ObjectGeneratorBuilder<T> builder(Class<T> clazz) {
        return new ObjectGeneratorBuilder<>(clazz);
    }

    public Stream<T> stream() {
        return Stream.generate(this::getOne);
    }

    public List<T> getMany(int size) {
        return stream()
                .limit(size)
                .collect(Collectors.toList());
    }

    public T getOne() {
        try {
            Constructor<T> constructor = clazz.getConstructor();
            T object = constructor.newInstance();
            ReflectionUtils.getFields(clazz)
                    .filter(field -> valueGenerator.supports(field.getType(), field.getName()))
                    .forEach(field -> fillValue(object, field));
            return object;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void fillValue(T object, Field field) {
        field.setAccessible(true);
        Object value = valueGenerator.generate(field.getType(), field.getName());
        try {
            field.set(object, value);
        } catch (IllegalAccessException ignored) {
        }
    }
}