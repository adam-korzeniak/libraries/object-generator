package com.adamkorzeniak.object_generator.suppliers;

class TypeSupplier implements Supplier {

    private final Class<?> type;
    private final java.util.function.Supplier<?> supplier;

    public TypeSupplier(Class<?> type, java.util.function.Supplier<?> supplier) {
        this.type = type;
        this.supplier = supplier;
    }

    @Override
    public Object generate(Class<?> clazz) {
        return supplier.get();
    }

    @Override
    public boolean canProvide(Class<?> clazz, String path) {
        return type == clazz;
    }
}
