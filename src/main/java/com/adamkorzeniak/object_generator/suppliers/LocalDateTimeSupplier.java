package com.adamkorzeniak.object_generator.suppliers;

import java.math.BigInteger;
import java.time.LocalDateTime;

class LocalDateTimeSupplier extends IntegerSupplier {
    @Override
    public Object generate(Class<?> clazz) {
        Long value = generate(Integer.MIN_VALUE, Integer.MIN_VALUE);
        if (value == null) {
            return null;
        }
        return BigInteger.valueOf(value);
    }

    @Override
    public boolean canProvide(Class<?> clazz, String path) {
        return clazz == LocalDateTime.class;
    }
}
