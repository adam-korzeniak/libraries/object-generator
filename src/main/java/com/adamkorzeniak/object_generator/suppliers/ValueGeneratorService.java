package com.adamkorzeniak.object_generator.suppliers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ValueGeneratorService {

    private final List<Supplier> suppliers = new ArrayList<>();

    public ValueGeneratorService(
            Map<String, java.util.function.Supplier<?>> pathSuppliers,
            Map<Class<?>, java.util.function.Supplier<?>> typeSuppliers,
            Set<String> acceptedPackages
    ) {
        pathSuppliers.entrySet().stream()
                .map(entry -> new PathSupplier(entry.getKey(), entry.getValue()))
                .forEach(suppliers::add);

        typeSuppliers.entrySet().stream()
                .map(entry -> new TypeSupplier(entry.getKey(), entry.getValue()))
                .forEach(suppliers::add);

        this.suppliers.add(new StringSupplier());
        this.suppliers.add(new IntPrimitiveSupplier());
        this.suppliers.add(new IntWrapperSupplier());
        this.suppliers.add(new LongPrimitiveSupplier());
        this.suppliers.add(new LongWrapperSupplier());
        this.suppliers.add(new BigIntegerSupplier());
        this.suppliers.add(new InnerClassSupplier(acceptedPackages));
    }

    public boolean supports(Class<?> clazz, String path) {
        return suppliers.stream()
                .anyMatch(supplier -> supplier.canProvide(clazz, path));
    }

    public <T> T generate(Class<T> clazz, String path) {
        return (T) suppliers.stream()
                .filter(supplier -> supplier.canProvide(clazz, path))
                .map(supplier -> supplier.generate(clazz))
                .findFirst()
                .orElse(null);
    }
}
