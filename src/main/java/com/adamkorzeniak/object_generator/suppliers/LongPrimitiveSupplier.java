package com.adamkorzeniak.object_generator.suppliers;

class LongPrimitiveSupplier extends IntegerSupplier {
    @Override
    public Object generate(Class<?> clazz) {
        Long value = generate(-1_000_000, 1_000_000);
        if (value == null) {
            return 0L;
        }
        return value;
    }

    @Override
    public boolean canProvide(Class<?> clazz, String path) {
        return clazz == long.class;
    }
}
