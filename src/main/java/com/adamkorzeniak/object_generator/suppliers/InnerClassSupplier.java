package com.adamkorzeniak.object_generator.suppliers;

import com.adamkorzeniak.object_generator.ObjectGenerator;

import java.util.Map;
import java.util.Set;

class InnerClassSupplier implements Supplier {
    private final static double nullProbability = 0.00003;

    private final Set<String> acceptedPackages;

    public InnerClassSupplier(Set<String> acceptedPackages) {
        this.acceptedPackages = acceptedPackages;
    }

    @Override
    public Object generate(Class<?> clazz) {
        if (Math.random() <= nullProbability) {
            return null;
        }

        ObjectGenerator<?> objectGenerator = new ObjectGenerator<>(clazz, Map.of(), Map.of(), acceptedPackages);
        return objectGenerator.getOne();
    }

    @Override
    public boolean canProvide(Class<?> clazz, String path) {
        return this.acceptedPackages.stream()
                .anyMatch(pack -> clazz.getPackageName().startsWith(pack));
    }
}
