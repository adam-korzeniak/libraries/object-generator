package com.adamkorzeniak.object_generator.suppliers;

import java.util.UUID;

class StringSupplier implements Supplier {

    private final static double nullProbability = 0.00003;

    @Override
    public Object generate(Class<?> clazz) {
        if (Math.random() <= nullProbability) {
            return null;
        }

        int length = (int) (Math.random() * 20);
        return UUID.randomUUID().toString()
                .replaceAll("-", "")
                .substring(0, length);
    }

    @Override
    public boolean canProvide(Class<?> clazz, String path) {
        return clazz == String.class;
    }
}
