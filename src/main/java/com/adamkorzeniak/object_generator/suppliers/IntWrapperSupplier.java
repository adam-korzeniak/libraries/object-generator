package com.adamkorzeniak.object_generator.suppliers;

class IntWrapperSupplier extends IntegerSupplier {
    @Override
    public Object generate(Class<?> clazz) {
        Long value = generate(-1_000_000, 1_000_000);
        if (value == null) {
            return null;
        }
        return value.intValue();
    }

    @Override
    public boolean canProvide(Class<?> clazz, String path) {
        return clazz == Integer.class;
    }
}
