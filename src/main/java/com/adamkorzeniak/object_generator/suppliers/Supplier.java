package com.adamkorzeniak.object_generator.suppliers;

interface Supplier {
    Object generate(Class<?> clazz);

    boolean canProvide(Class<?> clazz, String path);
}
