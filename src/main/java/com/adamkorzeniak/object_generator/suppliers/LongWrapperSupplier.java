package com.adamkorzeniak.object_generator.suppliers;

class LongWrapperSupplier extends IntegerSupplier {
    @Override
    public Object generate(Class<?> clazz) {
        Long value = generate(-1_000_000, 1_000_000);
        if (value == null) {
            return null;
        }
        return value;
    }

    @Override
    public boolean canProvide(Class<?> clazz, String path) {
        return clazz == Long.class;
    }
}
