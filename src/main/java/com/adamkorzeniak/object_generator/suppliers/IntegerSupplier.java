package com.adamkorzeniak.object_generator.suppliers;

abstract class IntegerSupplier implements Supplier {

    private final static double nullProbability = 0.00003;

    protected Long generate(Integer min, Integer max) {
        if (Math.random() <= nullProbability) {
            return null;
        }

        return (long) (Math.random() * (max - min) + min);
    }
}
