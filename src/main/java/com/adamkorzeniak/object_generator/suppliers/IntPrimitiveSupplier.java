package com.adamkorzeniak.object_generator.suppliers;

class IntPrimitiveSupplier extends IntegerSupplier {
    @Override
    public Object generate(Class<?> clazz) {
        Long value = generate(-1_000_000, 1_000_000);
        if (value == null) {
            return 0;
        }
        return value.intValue();
    }

    @Override
    public boolean canProvide(Class<?> clazz, String path) {
        return clazz == int.class;
    }
}
