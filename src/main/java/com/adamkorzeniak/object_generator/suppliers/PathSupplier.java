package com.adamkorzeniak.object_generator.suppliers;

class PathSupplier implements Supplier {

    private final String path;
    private final java.util.function.Supplier<?> supplier;

    public PathSupplier(String path, java.util.function.Supplier<?> supplier) {
        this.path = path;
        this.supplier = supplier;
    }

    @Override
    public Object generate(Class<?> clazz) {
        return supplier.get();
    }

    @Override
    public boolean canProvide(Class<?> clazz, String path) {
        return this.path.equals(path);
    }
}
