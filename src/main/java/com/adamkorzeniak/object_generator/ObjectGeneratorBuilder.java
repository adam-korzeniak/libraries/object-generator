package com.adamkorzeniak.object_generator;


import java.util.*;
import java.util.function.Supplier;

public class ObjectGeneratorBuilder<T> {

    private final Class<T> clazz;
    private final Map<String, Supplier<?>> pathSuppliers = new HashMap<>();
    private final Map<Class<?>, Supplier<?>> typeSuppliers = new HashMap<>();
    private final Set<String> acceptedPackages = new HashSet<>();

    ObjectGeneratorBuilder(Class<T> clazz) {
        this.clazz = clazz;
    }

    public ObjectGenerator<T> build() {
        return new ObjectGenerator<>(clazz, pathSuppliers, typeSuppliers, acceptedPackages);
    }

    public ObjectGeneratorBuilder<T> addPathSupplier(String name, Supplier<?> supplier) {
        pathSuppliers.put(name, supplier);
        return this;
    }

    public ObjectGeneratorBuilder<T> addPathSuppliers(Map<String, Supplier<?>> pathSuppliers) {
        this.pathSuppliers.putAll(pathSuppliers);
        return this;
    }

    public ObjectGeneratorBuilder<T> addTypeSupplier(Class<?> type, Supplier<?> supplier) {
        typeSuppliers.put(type, supplier);
        return this;
    }

    public ObjectGeneratorBuilder<T> addTypeSuppliers(Map<Class<?>, Supplier<?>> typeSuppliers) {
        this.typeSuppliers.putAll(typeSuppliers);
        return this;
    }

    public ObjectGeneratorBuilder<T> addAcceptedPackage(String acceptedPackage) {
        this.acceptedPackages.add(acceptedPackage);
        return this;
    }

    public ObjectGeneratorBuilder<T> addAcceptedPackages(Collection<String> acceptedPackages) {
        this.acceptedPackages.addAll(acceptedPackages);
        return this;
    }
}