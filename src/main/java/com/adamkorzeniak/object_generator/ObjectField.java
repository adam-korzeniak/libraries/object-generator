package com.adamkorzeniak.object_generator;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
class ObjectField {

    private final Class<?> type;
    private final String path;
}
